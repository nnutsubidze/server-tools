#!/usr/bin/env python

import os
import sys
import argparse
import subprocess
import shlex

def ap_hostname(args):
  """Change the hostname

  :param args: ArgumentParser arguments
  """
  cur_name = subprocess.check_output(shlex.split("uname -n")).strip()
  is_fqdn = False
  name = args.hostname
  fqdn = args.hostname
  names = '{} localhost'.format(name)
  if args.hostname.find('.') > 0:
    is_fqdn = True
    name = args.hostname.split('.', 1)[0]
    dnsname = args.hostname.split('.', 1)[1]
    names = ('{fqdn} {name} localhost.{dnsname} localhost'.format(
      fqdn=fqdn,
      name=name,
      dnsname=dnsname))

  print("Changing /etc/hosts...")
  ret = os.system("sed -i 's/^127.0.0.*/127.0.0.1 {}/g' /etc/hosts".format(
    names))
  if ret:
    exit("Failed to modify /etc/hosts. Ret: {}".format(ret))

  print("Changing /etc/hostname...")
  os.system("echo \"{}\" > /etc/hostname".format(name))

  print("Setting via hostname cmd...")
  os.system("hostname {}".format(name))

  minion_id = '/etc/salt/minion_id'
  if os.path.isfile(minion_id):
    print("Restarting salt-minion....")
    os.remove(minion_id)
    os.system("service salt-minion restart")

  print("You must logout for changes to take effect")

if __name__ == "__main__":
  def add_sp(sub_p, action, func=None, help=None):
    """Add an action to the main parser

    :param sub_p: The sub parser
    :param action: The action name
    :param func: The function to perform for this action
    :param help: The help to show for this action
    :rtype: The parser that is generated
    """
    p = sub_p.add_parser(action, help=help)
    if func:
      p.set_defaults(func=func)
    p.add_argument('-v', '--verbose', action='store_true',
             help='Show verbose logging')
    return p

  parser = argparse.ArgumentParser(description="Server Tools")
  sub_p = parser.add_subparsers(title='Actions',
    help='%(prog)s <action> -h for more info')

  p_hostname = add_sp(sub_p, "hostname",
    func=ap_hostname,
    help="Change the hostname")
  p_hostname.add_argument("hostname",
    help="The hostname to set")

  args = parser.parse_args()
  args.func(args)

